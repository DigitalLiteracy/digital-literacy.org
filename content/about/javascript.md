---
title: 'Javascript'
date: '2019-01-06'
slug: javascript
lastmod: '2019-01-06T13:51:57+01:00'
contentCopyright: false
---
# JavaScript Web Labels Table

More information on GNU JavaScript License Web Labels [here](https://www.gnu.org/licenses/javascript-labels.html). Want to set your JavaScript free as well? Go [here](https://www.gnu.org/software/librejs/free-your-javascript.html)!

Used identifiers are listed [here](https://www.gnu.org/software/librejs/manual/html_node/Free-Licenses-Detection.html#Free-Licenses-Detection). [SPDX identifiers](https://spdx.org/licenses/) are different sometimes.

{{< rawhtml >}}
    <table id="jslicense-labels1">
      <tr>
        <td><a href="/lib/jquery/jquery-3.2.1.min.js">jquery-3.2.1.min.js</a></td>
        <td><a href="http://www.jclark.com/xml/copying.txt">Expat</a></td>
        <td><a href="/lib/jquery/jquery-3.2.1.js">jquery-3.2.1.js</a></td>
      </tr>
      <tr>
        <td><a href="/lib/slideout/slideout-1.0.1.min.js">slideout-1.0.1.min.js</a></td>
        <td><a href="http://www.jclark.com/xml/copying.txt">Expat</a></td>
        <td><a href="/lib/slideout/slideout-1.0.1.js">slideout-1.0.1.js</a></td>
      </tr>
      <tr>
        <td><a href="/lib/mathjax/MathJax.js">MathJax.js</a></td>
        <td><a href="http://www.apache.org/licenses/LICENSE-2.0">Apache-2.0</a></td>
        <td><a href="/lib/mathjax/unpacked/MathJax.js">MathJax.js</a></td>
      </tr>
      <tr>
        <td><a href="/lib/mathjax/config/TeX-MML-AM_CHTML.js">TeX-MML-AM_CHTML.js</a></td>
        <td><a href="http://www.apache.org/licenses/LICENSE-2.0">Apache-2.0</a></td>
        <td><a href="/lib/mathjax/unpacked/config/TeX-MML-AM_CHTML.js">TeX-MML-AM_CHTML.js</a></td>
      </tr>
      <tr>
        <td><a href="/lib/mathjax/extensions/MathMenu.js">MathMenu.js</a></td>
        <td><a href="http://www.apache.org/licenses/LICENSE-2.0">Apache-2.0</a></td>
        <td><a href="/lib/mathjax/unpacked/extensions/MathMenu.js">MathMenu.js</a></td>
      </tr>
      <tr>
        <td><a href="/lib/mathjax/extensions/MathZoom.js">MathZoom.js</a></td>
        <td><a href="http://www.apache.org/licenses/LICENSE-2.0">Apache-2.0</a></td>
        <td><a href="/lib/mathjax/unpacked/extensions/MathZoom.js">MathZoom.js</a></td>
      </tr>
      <tr>
        <td><a href="/js/load-photoswipe.js">load-photoswipe.js</a></td>
        <td><a href="http://www.jclark.com/xml/copying.txt">Expat</a></td>
        <td><a href="/js/load-photoswipe.js">load-photoswipe.js</a></td>
      </tr>
      <tr>
        <td><a href="/lib/photoswipe/photoswipe-4.1.2.min.js">photoswipe-4.1.2.min.js</a></td>
        <td><a href="http://www.jclark.com/xml/copying.txt">Expat</a></td>
        <td><a href="/lib/photoswipe/photoswipe-4.1.2.js">photoswipe-4.1.2.js</a></td>
      </tr>
      <tr>
        <td><a href="/lib/photoswipe/photoswipe-ui-default-4.1.2.min.js">photoswipe-ui-default-4.1.2.min.js</a></td>
        <td><a href="http://www.jclark.com/xml/copying.txt">Expat</a></td>
        <td><a href="/lib/photoswipe/photoswipe-ui-default-4.1.2.js">photoswipe-ui-default-4.1.2.js</a></td>
      </tr>
      <tr>
        <td><a href="/js/highlight-9.13.1.pack.js">highlight-9.13.1.pack.js</a></td>
        <td><a href="http://opensource.org/licenses/BSD-3-Clause">BSD-3-Clause</a></td>
      </tr>
      <tr>
        <td><a href="/js/highlight-init.js">highlight-init.js</a></td>
        <td><a href="http://opensource.org/licenses/BSD-3-Clause">BSD-3-Clause</a></td>
        <td><a href="/js/highlight-init.js">highlight-init.js</a></td>
      </tr>
      <tr>
        <td><a href="/js/main.js">main.js</a></td>
        <td><a href="http://www.jclark.com/xml/copying.txt">Expat</a></td>
        <td><a href="/js/main.js">main.js</a></td>
      </tr>
    </table>
{{< /rawhtml >}}